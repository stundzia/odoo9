# -*- coding: utf-8 -*-

from openerp.tests import common


class TestPartner(common.TransactionCase):
    """Test cases for partner."""

    def setUp(self):
        """Set up data for partner."""
        super(TestPartner, self).setUp()
        self.partner_1 = self.browse_ref('base.res_partner_2')
        self.currency_eu = self.browse_ref('base.EUR')

    def test_magic_button(self):
        """Test magic button and default currency"""
        partner_2 = self.env['res.partner'].create({
            'name': 'test_partner'
            })
        self.assertEqual(
            partner_2.currency_id_2,
            self.currency_eu,
            "Wrong default currency assigned!"
            )
        self.partner_1.random_word_button()
        self.assertEqual(
            self.partner_1.some_word, "Hazaah!",
            "Magic button did not assign proper word"
            )
