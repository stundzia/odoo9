# -*- coding: utf-8 -*-

from openerp import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    some_word = fields.Char(string="Some Word")
    # The task required to create a relational field pointing
    # to the res.currency model, however a currency_id field
    # doing the exact same thing exists in the account module,
    # so to keep them separate and not risk breaking that functionality
    # we create a separate field.
    currency_id_2 = fields.Many2one(
        'res.currency', string="Currency",
        default=lambda self: self._get_default_currency()
        )

    @api.multi
    def random_word_button(self):
        word = "Hazaah!"
        self.some_word = word

    @api.model
    def _get_default_currency(self):
        return self.env.ref('base.EUR')
