# -*- coding: utf-8 -*-
# Author: Paulius Stundžia.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Partner form changes',
    'version': '0.2.0',
    'category': 'Base',
    'summary': 'base, partner, form',
    'description': """
    Adds some pointless field to the partner form :),
    adds a button to populate that field with the word
    "Hazaah!" and adds a currency field and sets it to EUR
    by default.
    """,
    'author': 'Paulius Stundžia',
    'depends': [
        'base'
    ],
    'data': [
        'views/partner_views.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
