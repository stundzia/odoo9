# -*- coding: utf-8 -*-
# Author: Paulius Stundžia.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Invoice/Refund menus',
    'version': '0.1.0',
    'category': 'Account',
    'summary': 'account, sale, security, menu',
    'description': """
    Adds customer and vendor refund menus. Adds a group that gives the right to
    cancel sale orders. Makes new products have the "Buy" and "Make to Order"
    routes by default.
    """,
    'author': 'Paulius Stundžia',
    'depends': [
        'account', 'sale', 'stock', 'purchase'
    ],
    'data': [
        'views/account_invoice_views.xml',
        'views/sale_order_views.xml',
        'security/res_groups.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
