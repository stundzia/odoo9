# -*- coding: utf-8 -*-

from openerp import fields, models, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    route_ids = fields.Many2many(
        default=lambda self: self._get_default_routes()
        )

    @api.model
    def _get_default_routes(self):
        routes = (
            self.env.ref('purchase.route_warehouse0_buy') |
            self.env.ref('stock.route_warehouse0_mto')
            )
        if routes:
            return routes.ids
        return []
