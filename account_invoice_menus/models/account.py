# -*- coding: utf-8 -*-

from openerp import models, api


class AccountInvoiceRefund(models.TransientModel):
    """Extend the invoice refund wizard."""

    _inherit = "account.invoice.refund"

    @api.multi
    def compute_refund(self, mode='refund'):
        res = super(AccountInvoiceRefund, self).compute_refund(mode)
        # TODO: this is a very non-elegant solution, need to return the
        # proper refund action. This has to be done, since the default
        # domain on invoice tree views no longer includes refunds.
        if type(res) != bool:
            # Check if super returned a result.
            if ('type', '=', 'in_invoice') in res.get('domain'):
                type_index = res['domain'].index(('type', '=', 'in_invoice'))
                res['domain'].pop(type_index)
            elif ('type', '=', 'out_invoice') in res.get('domain'):
                type_index = res['domain'].index(('type', '=', 'out_invoice'))
                res['domain'].pop(type_index)
        return res
